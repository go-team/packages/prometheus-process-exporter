#!/bin/sh
# kFreeBSD do not accept scripts as interpreters, using #!/bin/sh and sourcing.
if [ true != "$INIT_D_SCRIPT_SOURCED" ] ; then
    set "$0" "$@"; INIT_D_SCRIPT_SOURCED=true . /lib/init/init-d-script
fi
### BEGIN INIT INFO
# Provides:          prometheus-process-exporter
# Required-Start:    $remote_fs
# Required-Stop:     $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Prometheus exporter for processes metrics
# Description:       Prometheus exporter for processes metrics
### END INIT INFO

# Author: Guillem Jover <gjover@sipwise.com>

DESC="Prometheus exporter for processes metrics"
NAME=prometheus-process-exporter
USER=prometheus
GROUP=$USER
DAEMON=/usr/bin/$NAME
PIDFILE=/run/prometheus/$NAME.pid
LOGFILE=/var/log/prometheus/$NAME.log

START_ARGS="--no-close --background --make-pidfile"
STOP_ARGS="--remove-pidfile"

do_start_prepare()
{
  mkdir -p $(dirname $PIDFILE)
}

do_start_cmd_override()
{
  start-stop-daemon --start --quiet --oknodo \
    --exec $DAEMON --pidfile $PIDFILE --user $USER --group $GROUP \
    --chuid $USER:$GROUP $START_ARGS -- $ARGS >>$LOGFILE 2>&1
}

do_stop_cmd_override()
{
  start-stop-daemon --stop --quiet --oknodo --retry=TERM/30/KILL/5 \
    --exec $DAEMON --pidfile $PIDFILE --user $USER $STOP_ARGS
}
